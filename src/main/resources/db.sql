/*
 Navicat Premium Data Transfer

 Source Server         : localdb
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : localhost:3306
 Source Schema         : zhzzdev

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 17/12/2018 15:12:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BLOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `SCHED_NAME`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CRON_EXPRESSION` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TIME_ZONE_ID` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('schedulerFactory', 'jobTrigger', 'DEFAULT', '0/5 * * * * ? ', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('schedulerFactory', 'jobTrigger2', 'DEFAULT', '0/10 * * * * ? ', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ENTRY_ID` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `ENTRY_ID`) USING BTREE,
  INDEX `IDX_QRTZ_FT_TRIG_INST_NAME`(`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE,
  INDEX `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY`(`SCHED_NAME`, `INSTANCE_NAME`, `REQUESTS_RECOVERY`) USING BTREE,
  INDEX `IDX_QRTZ_FT_J_G`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_JG`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_T_G`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_FT_TG`(`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------
INSERT INTO `qrtz_fired_triggers` VALUES ('schedulerFactory', 'USER-20170707ZB15447566741431544756674128', 'jobTrigger', 'DEFAULT', 'USER-20170707ZB1544756674143', 1544756680040, 1544756685000, 0, 'ACQUIRED', NULL, NULL, '0', '0');

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_DURABLE` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_NONCONCURRENT` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IS_UPDATE_DATA` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_J_REQ_RECOVERY`(`SCHED_NAME`, `REQUESTS_RECOVERY`) USING BTREE,
  INDEX `IDX_QRTZ_J_GRP`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('schedulerFactory', 'jobDetail', 'DEFAULT', NULL, 'com.nf.nf.quartz.JobDetailBean', '1', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000C7461726765744F626A65637474000A7265706F72745461736B74000C7461726765744D6574686F6474000B70617274794E6F746963657800);
INSERT INTO `qrtz_job_details` VALUES ('schedulerFactory', 'jobDetail2', 'DEFAULT', NULL, 'com.nf.nf.quartz.JobDetailBean', '1', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000274000C7461726765744F626A65637474000B7265706F72745461736B3274000C7461726765744D6574686F6474000B70617274794E6F746963657800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LOCK_NAME` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `LOCK_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('schedulerFactory', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('schedulerFactory', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `INSTANCE_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `INSTANCE_NAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('schedulerFactory', 'USER-20170707ZB1544756674143', 1544756676664, 20000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `STR_PROP_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `STR_PROP_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `INT_PROP_1` int(11) NULL DEFAULT NULL,
  `INT_PROP_2` int(11) NULL DEFAULT NULL,
  `LONG_PROP_1` bigint(20) NULL DEFAULT NULL,
  `LONG_PROP_2` bigint(20) NULL DEFAULT NULL,
  `DEC_PROP_1` decimal(13, 4) NULL DEFAULT NULL,
  `DEC_PROP_2` decimal(13, 4) NULL DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `SCHED_NAME` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JOB_GROUP` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DESCRIPTION` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) NULL DEFAULT NULL,
  `PRIORITY` int(11) NULL DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `TRIGGER_TYPE` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) NULL DEFAULT NULL,
  `CALENDAR_NAME` varchar(190) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) NULL DEFAULT NULL,
  `JOB_DATA` blob NULL,
  PRIMARY KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_J`(`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_JG`(`SCHED_NAME`, `JOB_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_C`(`SCHED_NAME`, `CALENDAR_NAME`) USING BTREE,
  INDEX `IDX_QRTZ_T_G`(`SCHED_NAME`, `TRIGGER_GROUP`) USING BTREE,
  INDEX `IDX_QRTZ_T_STATE`(`SCHED_NAME`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_N_STATE`(`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_N_G_STATE`(`SCHED_NAME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_NEXT_FIRE_TIME`(`SCHED_NAME`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST`(`SCHED_NAME`, `TRIGGER_STATE`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_MISFIRE`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST_MISFIRE`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`, `TRIGGER_STATE`) USING BTREE,
  INDEX `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP`(`SCHED_NAME`, `MISFIRE_INSTR`, `NEXT_FIRE_TIME`, `TRIGGER_GROUP`, `TRIGGER_STATE`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('schedulerFactory', 'jobTrigger', 'DEFAULT', 'jobDetail', 'DEFAULT', NULL, 1544756685000, 1544756680000, 0, 'ACQUIRED', 'CRON', 1544756674000, 0, NULL, 0, '');
INSERT INTO `qrtz_triggers` VALUES ('schedulerFactory', 'jobTrigger2', 'DEFAULT', 'jobDetail2', 'DEFAULT', NULL, 1544756690000, 1544756680000, 0, 'WAITING', 'CRON', 1544756674000, 0, NULL, 0, '');

-- ----------------------------
-- Table structure for tb_sys_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_log`;
CREATE TABLE `tb_sys_log`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `module_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块',
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '描述',
  `method` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方法',
  `params` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '请求参数',
  `request_ip` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '访问ip',
  `del_flag` tinyint(1) NULL DEFAULT NULL COMMENT '状态（0：未逻辑删除 1：已逻辑删除）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_sys_log
-- ----------------------------
INSERT INTO `tb_sys_log` VALUES ('010b87f662564c64a356dae95e5f5c3b', '9f222da0693448099367025102e2069f', '2018-11-02 14:35:13', NULL, NULL, '登录', '登录系统', NULL, NULL, '0:0:0:0:0:0:0:1', NULL);
INSERT INTO `tb_sys_log` VALUES ('0c9dec516c454644943e728db4818332', '9f222da0693448099367025102e2069f', '2018-11-02 14:35:13', NULL, NULL, '登录', '登录系统', NULL, NULL, '0:0:0:0:0:0:0:1', NULL);
INSERT INTO `tb_sys_log` VALUES ('0e26fdecda5c47d0905bba9aa38d386c', '2', '2018-11-01 20:35:43', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('0e87da72a36b4d879cd7949272946abc', '9f222da0693448099367025102e2069f', '2018-12-04 15:55:26', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('2a724bc7cbb04b84b204d0d98ec40578', '9f222da0693448099367025102e2069f', '2018-12-04 16:05:25', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('37c4c1482173470d9a5674004d3fea5c', '2', '2018-11-01 20:35:38', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('42dd331cc0bb48d6b2e85455d5ebbf86', '2', '2018-11-01 20:35:43', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('4fc00b219e1f4a88b982dd834e4f4bc9', '9f222da0693448099367025102e2069f', '2018-11-02 14:33:36', NULL, NULL, '登录', '登录系统', NULL, NULL, '0:0:0:0:0:0:0:1', NULL);
INSERT INTO `tb_sys_log` VALUES ('6009144ebc324617a85f5cf8bf49a4e2', 'c687616dd7f74ac6a1d0b6a0c322b47e', '2018-11-01 20:50:18', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('6eb52a664555499cafdc03fbd51ee0b4', '9f222da0693448099367025102e2069f', '2018-11-27 19:46:35', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('7e496277299443539fcfab539a06c99d', '9f222da0693448099367025102e2069f', '2018-12-04 15:59:21', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('8073c239fc284144b1cd5800262a9273', '9f222da0693448099367025102e2069f', '2018-12-04 15:57:51', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('8273f8ea86c3482c86fe755985065e14', '2', '2018-11-01 20:35:39', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('84d4aaedecb54d76a6c984823d051501', '2', '2018-11-01 20:35:40', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('8d87788b48ad4c4581e5b7be1d13ce7a', '2', '2018-11-01 20:34:55', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('927d5b24856a46559d2942f7784e924a', '9f222da0693448099367025102e2069f', '2018-12-04 15:57:50', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('9d15b560837a4a10a1fff48909225d54', '9f222da0693448099367025102e2069f', '2018-11-28 16:11:28', NULL, NULL, '登录', '登录系统', NULL, NULL, '0:0:0:0:0:0:0:1', NULL);
INSERT INTO `tb_sys_log` VALUES ('a16e7483ae924abc80c0e5c98a3e8da3', '9f222da0693448099367025102e2069f', '2018-12-04 15:57:51', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('add79a1c9c184bb2ab30b704d15c050b', '2', '2018-11-01 20:34:55', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('c572e6c8cb254f52bf89eeb208d0f7c9', '9f222da0693448099367025102e2069f', '2018-12-04 15:57:44', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('c878cfa8dd5346fa8fcc1efc2d40c5ac', '9f222da0693448099367025102e2069f', '2018-12-04 16:05:41', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('c8b8522cb3db4f599802c6da43f71e3c', '9f222da0693448099367025102e2069f', '2018-11-02 11:23:04', NULL, NULL, '登录', '登录系统', NULL, NULL, '0:0:0:0:0:0:0:1', NULL);
INSERT INTO `tb_sys_log` VALUES ('dbcc5a8d0320475fad22e69fe83480d0', '2', '2018-11-01 20:35:39', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('ea048d4c3e96400da5a53d1a6d1699ee', '9f222da0693448099367025102e2069f', '2018-12-04 16:05:54', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('effdc4231c6842408a151e0245fd1c72', '2', '2018-11-01 20:35:40', NULL, NULL, '登录', '登录系统', NULL, NULL, '127.0.0.1', NULL);
INSERT INTO `tb_sys_log` VALUES ('faa7810ecb69494db85f58d229ea85ab', '9f222da0693448099367025102e2069f', '2018-11-02 14:35:14', NULL, NULL, '登录', '登录系统', NULL, NULL, '0:0:0:0:0:0:0:1', NULL);

-- ----------------------------
-- Table structure for tb_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_user`;
CREATE TABLE `tb_sys_user`  (
  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'id',
  `enable` tinyint(4) NULL DEFAULT 1 COMMENT '状态（0：禁用1：启用）',
  `create_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` timestamp(0) NULL DEFAULT NULL COMMENT '修改时间',
  `login_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `salt` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码盐',
  `token` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'token',
  `real_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '真实名字',
  `del_flag` tinyint(1) NULL DEFAULT 0 COMMENT '状态（0：未逻辑删除 1：已逻辑删除）',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_login_name`(`login_name`) USING BTREE COMMENT '用户名索引'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_sys_user
-- ----------------------------
INSERT INTO `tb_sys_user` VALUES ('2', 1, '1', '2018-05-14 13:35:05', '1', '2018-05-23 13:35:12', 'padmin11', '045d5739f8282e4cb041bc816959b3d7', '9f222da0693448099367025102e2069f', 'c116c20caa144bd1b0e0ba9d5c9ba1ea1541162142879', '管理员', 0);
INSERT INTO `tb_sys_user` VALUES ('2c0b60865cc4470fb4f33967f9310f6b', 1, NULL, NULL, NULL, NULL, 'admin6', '1fab30b2c653bc824e65835c09636c2c', '2c0b60865cc4470fb4f33967f9310f6b', 'string', 'string', 0);
INSERT INTO `tb_sys_user` VALUES ('42353e6776ef4ff181cb6e55310c503b', 1, NULL, NULL, NULL, NULL, 'admin1', '883b8b8ded036c64f91a4a07f486bf90', '42353e6776ef4ff181cb6e55310c503b', 'string', 'string', 0);
INSERT INTO `tb_sys_user` VALUES ('49b0e57344cc44078eeab1f8b24fc8e7', 1, NULL, NULL, NULL, NULL, 'admin2', '9a7ba8c4dc5796a5a8215a829ec0d984', '49b0e57344cc44078eeab1f8b24fc8e7', 'string', 'string', 0);
INSERT INTO `tb_sys_user` VALUES ('49e336ba81134bc8bf5644b62a063cd9', 1, NULL, NULL, NULL, NULL, 'string1', 'c796136f3a4eca129029862faa2f3605', '49e336ba81134bc8bf5644b62a063cd9', 'string', 'string', 0);
INSERT INTO `tb_sys_user` VALUES ('758c465ed75b4cdab131a73759e4d2df', 1, NULL, NULL, NULL, NULL, 'admin3', 'bfa3d94257458f8971e518f2802cef0a', '758c465ed75b4cdab131a73759e4d2df', 'string', 'string', 0);
INSERT INTO `tb_sys_user` VALUES ('7ff07bdb4b544a90808fc9d8908acd4d', 1, NULL, NULL, NULL, NULL, 'string12', 'df2a352a34dc46345d2178cf5bbba965', '7ff07bdb4b544a90808fc9d8908acd4d', 'string', 'string', 0);
INSERT INTO `tb_sys_user` VALUES ('886f7c99dacf44949353141b5eef0a33', 1, NULL, NULL, NULL, NULL, 'admin5', '96c36ae4378d2e78f2e3e46d628812b2', '886f7c99dacf44949353141b5eef0a33', 'string', 'string', 0);
INSERT INTO `tb_sys_user` VALUES ('9f222da0693448099367025102e2069f', 1, '1', '2018-05-14 13:35:05', '1', '2018-05-23 13:35:12', 'padmin', '045d5739f8282e4cb041bc816959b3d7', '9f222da0693448099367025102e2069f', '247fd181616449f3a20df25a8b070afe1543997154225', '管理员', 0);
INSERT INTO `tb_sys_user` VALUES ('b7bc41949f6c41cc86df321c31f64762', 1, NULL, NULL, NULL, NULL, 'admin4', '34a14b449d55f3013bf902ed4631151b', 'b7bc41949f6c41cc86df321c31f64762', 'string', 'string', 0);
INSERT INTO `tb_sys_user` VALUES ('c687616dd7f74ac6a1d0b6a0c322b47e', 1, NULL, NULL, NULL, NULL, 'string', 'b35b941d11df0418593657a54eb0f956', 'c687616dd7f74ac6a1d0b6a0c322b47e', '0e094cec320640d0921447275b02bde61541163018371', 'string', 0);

SET FOREIGN_KEY_CHECKS = 1;
