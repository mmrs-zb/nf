/**
 * 
 */
package com.nf.nf.common.exception;

/**
 * 
 */
@SuppressWarnings("serial")
public class InstanceException extends BaseException {
    public InstanceException() {
        super();
    }

    public InstanceException(Throwable t) {
        super(t);
    }


}
