package com.nf.nf.common.base;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

public class BaseServiceImpl<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> implements BaseService<M, T> {

    @Autowired
    private HttpServletRequest request;

    /**
     * 防止sql注入
     *
     * @param str
     * @return
     */
    private boolean sql_inj(String str) {

        String inj_str = "'|and|exec|insert|select|delete|update|count|*|%|chr|mid|master|truncate|char|declare|;|or|+|,";
        // SQL过滤，防止注入
        // String reg = "(?:')|(?:--)|(/\\*(?:.|[\\n\\r])*?\\*/)|"
        // +
        // "(\\b(select|update|and|or|delete|insert|trancate|char|into|substr|ascii|declare|exec|count|master|into|drop|execute)\\b)";
        // Pattern sqlPattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);
        // if (sqlPattern.matcher(str).find()) {
        // return false;
        // }

        String inj_stra[] = inj_str.split("\\|");

        for (int i = 0; i < inj_stra.length; i++) {
            if (str.indexOf(inj_stra[i]) >= 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取token
     *
     * @return
     */
    protected String getRequestToken() {
        String token = this.request.getHeader("token");
        if (StringUtils.isEmpty(token)) {
            return request.getParameter("token");
        }
        return token;
    }

    @Override
    public IPage<T> selectPageVo(Page page) {
        return null;
    }
}