package com.nf.nf.common.base;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 *
 * @param <M>
 * @param <T>
 */
public interface BaseService<M extends com.baomidou.mybatisplus.core.mapper.BaseMapper<T>, T> extends IService<T> {
    IPage<T> selectPageVo(Page page);
}