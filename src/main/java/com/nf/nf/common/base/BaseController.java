package com.nf.nf.common.base;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.IService;
import com.nf.nf.common.core.Constants;
import com.nf.nf.common.support.ApiData;
import com.nf.nf.mapper.TbSysUserMapper;
import com.nf.nf.model.TbSysUser;
import com.nf.nf.util.IdGen;
import com.nf.nf.util.UserUtils;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * <p>
 * 基础控制器
 * </p>
 *
 * @author cris
 * @Date 2018-05-03
 */
// extends SuperController implements HandlerInterceptor
public class BaseController {

    protected HttpServletRequest request;

    protected HttpServletResponse response;


    @Resource
    private TbSysUserMapper tbSysUserMapper;

    @ModelAttribute
    public void init(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }


    /**
     * 获取token
     *
     * @return
     */
    protected String getRequestToken() {
        String token = this.request.getHeader("token");
        if (StringUtils.isEmpty(token)) {
            return request.getParameter("token");
        }
        return token;
    }

    /**
     * 获得系统当前登录用户
     *
     * @return 返回当前用户对象
     */
    public TbSysUser getUser() {
        String token = getRequestToken();
        TbSysUser usertk = new TbSysUser();
        usertk.setToken(token);

        TbSysUser user = tbSysUserMapper.selectOne(new QueryWrapper<TbSysUser>(usertk));
        return user;
    }


    protected String booleanToString(boolean rlt) {
        return rlt ? "true" : "false";
    }

    /**
     * api接口请求返回参数封装类
     *
     * @return
     */
    public ApiData getSuccessApiData() {
        ApiData data = getApiData();
        data.setResult(1);
        return data;
    }

    /**
     * api接口请求返回参数封装类
     *
     * @return
     */
    public ApiData getSuccessApiData(Object data) {
        ApiData apiData = getApiData();
        apiData.setResult(1);
        apiData.setData(data);
        return apiData;
    }

    /**
     * api接口请求返回参数封装类
     *
     * @return
     */
    public ApiData getApiData() {
        return new ApiData();
    }

    /**
     * api接口请求返回参数封装类
     *
     * @return
     */
    public ApiData getFailApiData(String msg) {
        ApiData data = getApiData();
        data.setResult(0);
        data.setMsg(msg);
        return data;
    }

    /***
     * 返回http://hostname:port @return @throws
     */
    protected String getRootPath() {
        String basePath = this.request.getContextPath();
        String headerRefer = this.request.getHeader("Referer");
        String rootPath = null;
        if (headerRefer != null && headerRefer.length() > 0) {
            rootPath = headerRefer.substring(0, headerRefer.indexOf(basePath) + 1);
        }
        return rootPath;
    }

    /***
     * 返回http://hostname:port/projectname @return @throws
     */
    protected String getBasePath() {
        return this.request.getContextPath();
    }

    protected ApiData commonInsert(IService baseService, BaseModel param) {
        ApiData data = getSuccessApiData();
        Boolean re = false;
        param.setId(IdGen.uuid());
        UserUtils.setCommonFieldValue(param, 0);

        if (StringUtils.isEmpty(param.getCreateBy())) {
            String userId = getUser().getId();
            param.setCreateBy(userId);
        }
        if (StringUtils.isEmpty(param.getUpdateBy())) {
            String userId = getUser().getId();
            param.setUpdateBy(userId);
        }
        re = baseService.save(param);

        if (!re) {
            data.setResult(Constants.reqFail);
        }
        return data;
    }

    protected ApiData commonUpdate(IService baseService, BaseModel param) {
        ApiData data = getSuccessApiData();
        Boolean re = false;
        UserUtils.setCommonFieldValue(param, 2);
        if (StringUtils.isEmpty(param.getUpdateBy())) {
            String userId = getUser().getId();
            param.setUpdateBy(userId);
        }
        re = baseService.updateById(param);

        if (!re) {
            data.setResult(Constants.reqFail);
        }
        return data;
    }

    protected ApiData commonDetail(IService baseService, String id) {
        ApiData data = getSuccessApiData();
        Object re = baseService.getById(id);
        data.setData(re);
        return data;
    }


}