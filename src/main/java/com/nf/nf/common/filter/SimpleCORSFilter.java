package com.nf.nf.common.filter;

import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Athoer 张奔
 * @Desc 过滤器配置
 */
//比较核心的代码是自定义类上面加上@WebFilter，其中@Order注解表示执行过滤顺序，值越小，越先执行
@Order(1)
@WebFilter(filterName = "SimpleCORSFilter", urlPatterns = "/*")
public class SimpleCORSFilter implements Filter {
    
    @Override  
    public void destroy() {  
          
    }  
  
    @Override  
    public void doFilter(ServletRequest req, ServletResponse res,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods",
                "GET, POST, DELETE, PUT, OPTIONS");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers",
                "Access-Control-Allow-Origin, Cache-Control,Pragma,Expires,accept,x-requested-with,content-type, token");
        chain.doFilter(req, res); 
          
    }  
  
    @Override  
    public void init(FilterConfig arg0) throws ServletException {
          
    }  
  
}