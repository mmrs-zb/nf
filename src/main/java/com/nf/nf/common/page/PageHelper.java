package com.nf.nf.common.page;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.type.Alias;

import java.util.HashMap;
import java.util.Map;

@Alias("ph")
public class PageHelper<T> extends Page {
    private T t;

    private Map<String, Object> cm = new HashMap<String, Object>();

    public Map<String, Object> getCm() {
        return cm;
    }

    public void setCm(Map<String, Object> cm) {
        this.cm = cm;
    }

}

