package com.nf.nf.common.core;

import com.nf.nf.util.InstanceUtil;

import java.util.Map;

/**
 * 常量表
 * 
 * @version $Id: Constants.java, v 0.1 2014-2-28 上午11:18:28  Exp $
 */
public interface Constants {
	/**
	 * 异常信息统一头信息<br>
	 * 非常遗憾的通知您,程序发生了异常
	 */
	static final String Exception_Head = "OH,MY GOD! SOME ERRORS OCCURED! AS FOLLOWS :";
	/** 缓存键值 */
	static final Map<Class<?>, String> cacheKeyMap = InstanceUtil.newHashMap();
	/** 操作名称 */
	static final String OPERATION_NAME = "OPERATION_NAME";
	/** 客户端语言 */
	static final String USERLANGUAGE = "userLanguage";
	/** 客户端主题 */
	static final String WEBTHEME = "webTheme";
	/** 当前用户 */
	static final String CURRENT_USER = "CURRENT_USER";
	/** 客户端信息 */
	static final String USER_AGENT = "USER-AGENT";
	/** 客户端信息 */
	static final String USER_IP = "USER_IP";
	/** 上次请求地址 */
	static final String PREREQUEST = "PREREQUEST";
	/** 上次请求时间 */
	static final String PREREQUEST_TIME = "PREREQUEST_TIME";
	/** 登录地址 */
	static final String LOGIN_URL = "/login.html";
	/** 非法请求次数 */
	static final String MALICIOUS_REQUEST_TIMES = "MALICIOUS_REQUEST_TIMES";
	/** 缓存命名空间 */
	public static final String CACHE_NAMESPACE = "BTGCP:";
	/** 在线用户数量 */
	static final String ALLUSER_NUMBER = "SYSTEM:" + CACHE_NAMESPACE + "ALLUSER_NUMBER";
	/** TOKEN */
	static final String TOKEN_KEY = CACHE_NAMESPACE + "TOKEN_KEY";
	
	/** 接口参数secretId */
	static final String SECRET_ID = "secretId";
	
	/** 接口参数secretKey */
	static final String SECRET_KEY = "secretKey";
	
	/** 接口参数Signature */
    static final String SIGNATURE = "signature";
    
    /** 接口参数TimeStamp */
    static final String TIMESTAMP = "timestamp";
    
	static final String QUERY_SEPARATOR = "&";
	
	/** 半角冒号*/
	public static final String COLON = ":";
	
	/** 空字符串*/
	public static final String BLANK = "";
	
	/** 横线*/
	public static final String LINE = "-";
	
	/** 数据逻辑标志位（1：正常）*/
	public static final int ENABLE_NORMAL = 1;
	
	/** 数据逻辑标志位（1：删除）*/
	public static final int ENABLE_DEL = 0;
	
	/** HTTP协议 */
	public static final String HTTP_PROTOCOL = "http://";
	
	/** HTTPS协议 */
	public static final String HTTPS_PROTOCOL = "https://";

	/** USER_SESSION 对应用户session中保存的用户信息 key 值 */
	public static final String USER_SESSION = "USER_SESSION";

	public static String sms_cd_key_value = "EUCP-EMY-SMS1-22EL6";

	public static String sms_password_value = "A792FD024B87A71E";

	public static final String COMMA_SEPARATOR = ",";

	/** 默认密码 */
	public static final String PASSWORD = "123456";

	/* ----------------- 请求响应中的结果 - start ------------------ */

	public static final Integer reqSuccess = 1;

	public static final Integer reqFail = 0;

	/* ----------------- 请求响应中的结果 - end ------------------ */

	public static final String menuLogin = "登录";

}
