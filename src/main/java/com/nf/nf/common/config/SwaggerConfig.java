package com.nf.nf.common.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger配置
 */
@EnableWebMvc
@EnableSwagger2
@Configuration
public class SwaggerConfig {
    private static List<Parameter> pars = new ArrayList<Parameter>();

    static {
        ParameterBuilder tokenPar = new ParameterBuilder();
        tokenPar.name("token").description("令牌").modelRef(new ModelRef("string")).parameterType("header")
                .required(false).build();
        pars.add(tokenPar.build());
    }

    
    /*** --- 公共 start --- ***/
    @Bean
    public Docket createRestApi1() {

        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo1()).select()
                .apis(RequestHandlerSelectors.basePackage("com.nf.nf.controller")) // 注意修改此处的包名
                .build().groupName("common").globalOperationParameters(pars);
    }


    private ApiInfo apiInfo1() {
        return new ApiInfoBuilder().title("接口列表 v1.0")
                .description("接口测试")
                .termsOfServiceUrl("http://url/common/swagger-ui.html") // 将“url”换成自己的ip:port
                .contact("cris") // 无所谓（这里是作者的别称）
                .version("1.0").build();
    }

    /*** --- 公共 end --- ***/

//    /*** --- rs start --- ***/
//    @Bean
//    public Docket createRestApi2() {
//
//        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo2()).select()
//                .apis(RequestHandlerSelectors.basePackage("com.ccdc.pcloudsoft.controller.rs")) // 注意修改此处的包名
//                .paths(PathSelectors.regex("/rs*.*")).build().groupName("rs").globalOperationParameters(pars);
//    }
//
//    // .paths(PathSelectors.any())
//    // .paths(PathSelectors.regex("/rs/.*"))
//    private ApiInfo apiInfo2() {
//        return new ApiInfoBuilder().title("人事接口列表 v1.0") // 任意，请稍微规范点
//                .description("人事接口测试") // 任意，请稍微规范点
//                .termsOfServiceUrl("http://url/rs/swagger-ui.html") // 将“url”换成自己的ip:port
//                .contact("cris") // 无所谓（这里是作者的别称）
//                .version("1.0").build();
//    }
//    /*** --- rs end --- ***/

//    /*** --- dj start --- ***/
//    @Bean
//    public Docket createRestApi3() {
//
//        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo3()).select()
//                .apis(RequestHandlerSelectors.basePackage("com.ccdc.pcloudsoft.controller.dj")) // 注意修改此处的包名
//                .build().groupName("dj").globalOperationParameters(pars);
//    }
//
//    // .paths(PathSelectors.any())
//    // .paths(PathSelectors.regex("/rs/.*"))
//    private ApiInfo apiInfo3() {
//        return new ApiInfoBuilder().title("党建接口列表 v1.0") // 任意，请稍微规范点
//                .description("党建接口测试") // 任意，请稍微规范点
//                .termsOfServiceUrl("http://url/dj/swagger-ui.html") // 将“url”换成自己的ip:port
//                .contact("cris") // 无所谓（这里是作者的别称）
//                .version("1.0").build();
//    }
//    /*** --- rs end --- ***/

//    @Api：修饰整个类，描述Controller的作用
//    @ApiOperation：描述一个类的一个方法，或者说一个接口
//    @ApiParam：单个参数描述
//    @ApiModel：用对象来接收参数
//    @ApiProperty：用对象接收参数时，描述对象的一个字段
//    @ApiResponse：HTTP响应其中1个描述
//    @ApiResponses：HTTP响应整体描述
//    @ApiIgnore：使用该注解忽略这个API
//    @ApiError ：发生错误返回的信息
//    @ApiImplicitParam：一个请求参数
//    @ApiImplicitParams：多个请求参数
}