package com.nf.nf;

import com.nf.nf.quartz.JobDetailBean;
import com.nf.nf.quartz.ReportTask;
import org.quartz.JobDataMap;
import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.expression.ParseException;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import javax.sql.DataSource;

/**
 * @Author: cris
 * @Description: Quartz配置
 * @Date 2018/12/14 10:34
 * @ClassName: QuartzConfig
 */
@Configuration
public class QuartzConfig {

    @javax.annotation.Resource
    private DataSource dataSource;

    @Bean(name="jobDetail")
    public JobDetailFactoryBean detailFactoryBean(ReportTask reportTask){
        JobDetailFactoryBean jobDetail = new JobDetailFactoryBean();
        //是否并发执行
        jobDetail.setJobClass(JobDetailBean.class);
        jobDetail.setDurability(true);
        JobDataMap jobDataAsMap = new JobDataMap();
        jobDataAsMap.put("targetObject", "reportTask" );
        jobDataAsMap.put("targetMethod", "partyNotice");
        jobDetail.setJobDataAsMap(jobDataAsMap);
        return jobDetail;
    }

    @Bean(name = "jobTrigger")
    public CronTriggerFactoryBean cronJobTrigger(@Qualifier("jobDetail")JobDetailFactoryBean jobDetail) {
        CronTriggerFactoryBean tigger = new CronTriggerFactoryBean();
        tigger.setJobDetail(jobDetail.getObject());
        try {
            tigger.setCronExpression("0/5 * * * * ? ");// 每5秒执行一次
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return tigger;

    }


    @Bean(name="jobDetail2")
    public JobDetailFactoryBean detailFactoryBean2(ReportTask reportTask){
        JobDetailFactoryBean jobDetail = new JobDetailFactoryBean();
        //是否并发执行
        jobDetail.setJobClass(JobDetailBean.class);
        jobDetail.setDurability(true);
        JobDataMap jobDataAsMap = new JobDataMap();
        jobDataAsMap.put("targetObject", "reportTask2" );
        jobDataAsMap.put("targetMethod", "partyNotice");
        jobDetail.setJobDataAsMap(jobDataAsMap);
        return jobDetail;
    }

    @Bean(name = "jobTrigger2")
    public CronTriggerFactoryBean cronJobTrigger2(@Qualifier("jobDetail2") JobDetailFactoryBean jobDetail2) {
        CronTriggerFactoryBean tigger = new CronTriggerFactoryBean();
        tigger.setJobDetail(jobDetail2.getObject());
        try {
            tigger.setCronExpression("0/10 * * * * ? ");// 每10秒执行一次
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return tigger;

    }

    @Bean
    public SchedulerFactoryBean schedulerFactory(Trigger... jobTriggers) {
        SchedulerFactoryBean bean = new SchedulerFactoryBean();
        //用于quartz集群,QuartzScheduler 启动时更新己存在的Job
        bean.setOverwriteExistingJobs(true);
        //用户Quartz集群
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource resource = resourceLoader.getResource("quartz.properties");
        bean.setConfigLocation(resource);
        bean.setDataSource(dataSource);
        //延时启动，应用启动20秒后
        bean.setStartupDelay(1);
        //注册触发器
        bean.setTriggers(jobTriggers);
        bean.setOverwriteExistingJobs(true);
        bean.setApplicationContextSchedulerContextKey("applicationContext");
        return bean;
    }
}
