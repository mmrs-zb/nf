package com.nf.nf;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ServletComponentScan("com.nf.nf.common.filter")
@MapperScan(basePackages = {"com.nf.nf.mapper.**"})
@EnableScheduling
public class NfApplication {

    public static void main(String[] args) {
        SpringApplication.run(NfApplication.class, args);
    }


}
