package com.nf.nf.model;

import com.nf.nf.common.base.BaseModel;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * <p>
 * 日志表
 * </p>
 *
 * @author cris
 * @since 2018-10-29
 */
@SuppressWarnings("serial")
@ApiModel(value = "TbSysLog", description = "日志表")
public class TbSysLog extends BaseModel {

    /**
     * 模块
     */
    @ApiModelProperty(value = "模块")
    private String moduleName;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
    /**
     * 方法
     */
    @ApiModelProperty(value = "方法")
    private String method;
    /**
     * 请求参数
     */
    @ApiModelProperty(value = "请求参数")
    private String params;
    /**
     * 访问ip
     */
    @ApiModelProperty(value = "访问ip")
    private String requestIp;


    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getRequestIp() {
        return requestIp;
    }

    public void setRequestIp(String requestIp) {
        this.requestIp = requestIp;
    }


    @Override
    public String toString() {
        return "TbSysLog{" +
                "moduleName=" + moduleName +
                ", description=" + description +
                ", method=" + method +
                ", params=" + params +
                ", requestIp=" + requestIp +
                "}";
    }
}