package com.nf.nf.model;

import io.swagger.annotations.ApiModelProperty;

/**
 * @Author: cris
 * @Description: 重置密码实体类
 * @Date 2018/11/27 19:57
 * @ClassName: ResetPwdDto
 */
public class ResetPwdDto {
    @ApiModelProperty("需要重置的用户id")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
