package com.nf.nf.model;

import com.nf.nf.common.base.BaseModel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * <p>
 * 系统用户表
 * </p>
 *
 * @author cris
 * @since 2018-09-26
 */
@SuppressWarnings("serial")
@ApiModel(value = "TbSysUser", description = "系统用户表")
public class TbSysUser extends BaseModel {

    /**
     * 状态（0：禁用1：启用）
     */
    @ApiModelProperty(value = "状态（0：禁用1：启用）")
    private Integer enable;
    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String loginName;
    /**
     * 密码
     */
    @ApiModelProperty(value = "密码")
    private String password;
    /**
     * 密码盐
     */
    @ApiModelProperty(value = "密码盐")
    private String salt;
    /**
     * token
     */
    @ApiModelProperty(value = "token")
    private String token;
    /**
     * 真实名字
     */
    @ApiModelProperty(value = "真实名字")
    private String realName;


    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }


    @Override
    public String toString() {
        return "TbSysUser{" +
                "enable=" + enable +
                ", loginName=" + loginName +
                ", password=" + password +
                ", salt=" + salt +
                ", token=" + token +
                ", realName=" + realName +
                "}";
    }
}