package com.nf.nf.model;

import io.swagger.annotations.ApiModelProperty;

/**
 * @Author: cris
 * @Description: 修改密码的实体
 * @Date 2018/10/9 10:54
 * @ClassName: ModifyPwdDto
 */
public class ModifyPwdDto {
    @ApiModelProperty("老密码")
    private String pwdOld;

    @ApiModelProperty("新密码")
    private String pwdNew;

    public String getPwdOld() {
        return pwdOld;
    }

    public void setPwdOld(String pwdOld) {
        this.pwdOld = pwdOld;
    }

    public String getPwdNew() {
        return pwdNew;
    }

    public void setPwdNew(String pwdNew) {
        this.pwdNew = pwdNew;
    }
}
