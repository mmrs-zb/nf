package com.nf.nf.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @Author: cris
 * @Description: 登陆
 * @Date 2018/11/1 20:12
 * @ClassName: Login
 */
@ApiModel
public class Login implements Serializable {
	@ApiModelProperty("登录用户名")
    private String account;
	
	@ApiModelProperty("登陆密码（选择密码登陆的时候传入）")
    private String password;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
