package com.nf.nf.quartz;


import com.nf.nf.util.DateUtils;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

@Component
public class ReportTask implements Serializable {
    

    /** logger */
//    protected final static Logger logger = Logger.getLogger(ReportTask.class);

    /**
     * 处理党组织换届与预备党员转正提醒
     * 
     * @throws Exception
     */
    public void partyNotice() {

        System.out.println("....1.........."
                + DateUtils.date2Str(new Date(), "yyyy-MM-dd HH:mm:ss"));

    }
    
}
