package com.nf.nf.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Field;
import java.util.Date;

/**
 * 用户工具类
 */
public class UserUtils {

    protected final Logger logger = LoggerFactory.getLogger(UserUtils.class);

    /**
     * 判断字符串是否为空
     *
     * @param o   实体类
     * @param flg 0：全部设定；1：设定创建相关；2：设定更新相关
     * @return
     */
    public static void setCommonFieldValue(Object o, int flg) {
        try {
            // 从session里面获取登录的信息
            UserUtils u = new UserUtils();
//			String id = u.getu();
            String id = getUserIdFromSession();
            Date date = new Date();
            if (flg == 0) {
                // 创建者
                Field field = getDeclaredField(o, "createBy");
                field.setAccessible(true);
                field.set(o, id);
                // 更新者
                field = getDeclaredField(o, "updateBy");
                field.setAccessible(true);
                field.set(o, id);
                // 创建时间
                field = getDeclaredField(o, "createTime");
                field.setAccessible(true);
                field.set(o, date);
                // 更新时间
                field = getDeclaredField(o, "updateTime");
                field.setAccessible(true);
                field.set(o, date);
                // 状态
                field = getDeclaredField(o, "delFlag");
                field.setAccessible(true);
                field.set(o, 0);
            } else if (flg == 1) {
                // 创建者
                Field field = getDeclaredField(o, "createBy");
                field.setAccessible(true);
                field.set(o, id);
                // 创建时间
                field = getDeclaredField(o, "createTime");
                field.setAccessible(true);
                field.set(o, date);
            } else if (flg == 2) {
                // 更新者
                Field field = getDeclaredField(o, "updateBy");
                field.setAccessible(true);
                field.set(o, id);
                // 更新时间
                field = getDeclaredField(o, "updateTime");
                field.setAccessible(true);
                field.set(o, date);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 循环向上转型, 获取对象的 DeclaredField
     *
     * @param object    : 子类对象
     * @param fieldName : 父类中的属性名
     * @return 父类中的属性对象
     */
    public static Field getDeclaredField(Object object, String fieldName) {
        Field field = null;
        Class<?> clazz = object.getClass();
        for (; clazz != Object.class; clazz = clazz.getSuperclass()) {
            try {
                field = clazz.getDeclaredField(fieldName);
                return field;
            } catch (Exception e) {
                // 这里甚么都不要做！并且这里的异常必须这样写，不能抛出去。
                // 如果这里的异常打印或者往外抛，则就不会执行clazz =
                // clazz.getSuperclass(),最后就不会进入到父类中了
            }
        }
        return null;
    }

    /**
     * 从session中取得当前用户登录id
     *
     * @return
     */
    public static String getUserIdFromSession() {

        HttpServletRequest req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        if (null == RequestContextHolder.getRequestAttributes()) {
            return "定时任务";
        }
        if (null == req) {
            return "定时任务";
        }
        HttpSession session = req.getSession();
        if (null == session) {
            return "定时任务";
        }
        return UserUtils.objectToString(session.getAttribute("id"));
    }

    /**
     * object类型转String
     *
     * @param object
     * @return
     */
    public static String objectToString(Object object) {

        if (null == object) {
            return "";
        }
        return object.toString();
    }
}
