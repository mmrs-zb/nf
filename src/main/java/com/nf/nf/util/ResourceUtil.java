package com.nf.nf.util;

import javax.servlet.http.HttpServletRequest;
import java.util.*;


/**
 * 项目参数工具类
 * 
 * @author  章隆敏
 * @version  [1.0, 2017年1月6日]
 * @since  [pcloud/2.0]
 */
public class ResourceUtil {
	private static final ResourceBundle bundle = ResourceBundle.getBundle("config");
	private static final ResourceBundle errorBundle = ResourceBundle.getBundle("error");

	/**
	 * 获得请求路径
	 * 
	 * @param request
	 * @return
	 */
	public static String getRequestPath(HttpServletRequest request) {
		String requestPath = null;
		requestPath = request.getRequestURI() ;
		requestPath = requestPath.substring(request.getContextPath().length() + 1);// 去掉项目路径
		return requestPath;
	}

	/**
	 * 获取配置文件参数
	 * 
	 * @param name
	 * @return
	 */
	public static final String getConfigByName(String name) {
		return bundle.getString(name);
	}

	/**
	 * @methodName: 获取错误编码文件参数
	 * @description: TODO
	 * <br/>
	  * @param Code
	 * @return
	 * @author cris
	 * @date 2018/9/27 15:27
	 */
	public static final String getErrorByCode(String Code) {
		return errorBundle.getString(Code);
	}
	

	/** 
	 * @methodName: getConfigMap
	 * @description: 获取配置文件参数
	 * <br/>
	  * @param path
	 * @return 
	 * @author cris
	 * @date 2018/9/27 15:18
	 */
	public static final Map<Object, Object> getConfigMap(String path) {
		ResourceBundle bundle = ResourceBundle.getBundle(path);
		Set set = bundle.keySet();
		return setToMap(set);
	}
	
	/** 
	 * @methodName: setToMap
	 * @description: SET转换MAP
	 * <br/>
	  * @param setobj
	 * @return 
	 * @author cris
	 * @date 2018/9/27 15:17
	 */
	@SuppressWarnings("unchecked")
	public static Map<Object, Object> setToMap(Set<Object> setobj) {
		Map<Object, Object> map = new HashMap<Object,Object>();
		for (Iterator<Object> iterator = setobj.iterator(); iterator.hasNext();) {
			Map.Entry<Object, Object> entry = (Map.Entry<Object, Object>) iterator.next();
			map.put(entry.getKey().toString(), entry.getValue() == null ? "" : entry.getValue().toString().trim());
		}
		return map;

	}


}
