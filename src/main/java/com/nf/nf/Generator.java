package com.nf.nf;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * 代码生成
 * 
 */
public class Generator {
    private static final String PROJECT_PATH = System.getProperty("user.dir");// 项目在硬盘上的基础路径

    private static final String JAVA_PATH = "/src/main/java"; // java文件路径
    // public static final String BASE_PACKAGE =
    // "com.ccdc.pcloudsoft";//生成代码所在的基础包名称，可根据自己公司的项目修改（注意：这个配置修改之后需要手工修改src目录项目默认的包路径，使其保持一致，不然会找不到类）

    /**
     * 测试 run 执行 注意：不生成service接口 注意：不生成service接口 注意：不生成service接口
     * <p>
     * </p>
     */
    public static void main(String[] args) {

        System.out.println("" + PROJECT_PATH);
        String path = PROJECT_PATH + JAVA_PATH;
        // String realPath = path + packageConvertPath(BASE_PACKAGE);
        String realPath = path;
        System.out.println(realPath);
        // String[] tableNames = getAllTableName();

        // String[] tableNames = getAllTableName();
        AutoGenerator mpg = new AutoGenerator();
        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        gc.setOutputDir(realPath);
//        gc.setOutputDir("D://");
        gc.setFileOverride(false);
        gc.setActiveRecord(false);
        gc.setEnableCache(false);// XML 二级缓存
        gc.setBaseResultMap(false);// XML ResultMap
        gc.setBaseColumnList(false);// XML columList
        gc.setOpen(false);
        gc.setAuthor("cris");
        gc.setBaseColumnList(true);
        gc.setBaseResultMap(true);
        // 自定义文件命名，注意 %s 会自动填充表实体属性！
        // gc.setMapperName("%sDao");
        // gc.setXmlName("%sDao");
        gc.setServiceImplName("%sServiceImpl");
        gc.setServiceName("%sService");
        // gc.setServiceImplName("%sServiceDiy");
        // gc.setControllerName("%sAction");
        mpg.setGlobalConfig(gc);
        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDbType(DbType.MYSQL);
        dsc.setDriverName("com.mysql.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("root123456");
        dsc.setUrl("jdbc:mysql://127.0.0.1:3306/zhzzdev?characterEncoding=utf8");
        mpg.setDataSource(dsc);
        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        // strategy.setTablePrefix("sys_");// 此处可以修改为您的表前缀
        strategy.setNaming(NamingStrategy.underline_to_camel);// 表名生成策略
        strategy.setInclude(new String[] { "tb_sys_log"}); // 需要生成的表
        // strategy.setTablePrefix(new String[] { "tb_" });// 此处可以修改为您的表前缀
        // strategy.setExclude(new String[]{"test"}); // 排除生成的表
        // 自定义实体父类
        strategy.setSuperEntityClass("com.nf.nf.common.base.BaseModel");
        // 自定义实体，公共字段
        strategy.setSuperEntityColumns(
                new String[] { "id", "del_flag", "create_by", "create_time", "update_by", "update_time" });
        // 自定义 mapper 父类
        strategy.setSuperMapperClass("com.nf.nf.common.base.BaseMapper");
        // 自定义 service 父类
        strategy.setSuperServiceClass("com.nf.nf.common.base.BaseService");
        // 自定义 service 实现类父类
        strategy.setSuperServiceImplClass("com.nf.nf.common.base.BaseServiceImpl");
        // 自定义 controller 父类
        strategy.setSuperControllerClass("com.nf.nf.common.base.BaseController");
        // 【实体】是否生成字段常量（默认 false）
        // public static final String ID = "test_id";
        // strategy.setEntityColumnConstant(true);
        // 【实体】是否为构建者模型（默认 false）
        // public User setName(String name) {this.name = name; return this;}
        // strategy.setEntityBuliderModel(true);
        mpg.setStrategy(strategy);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.nf.nf");

        pc.setEntity("model");
        pc.setMapper("mapper");
        pc.setXml("mapper.xml");
        pc.setServiceImpl("service.impl");
        pc.setService("service");
        pc.setController("controller");

        mpg.setPackageInfo(pc);
        // // 注入自定义配置，可以在 VM 中使用 cfg.abc 设置的值
        // InjectionConfig cfg = new InjectionConfig() {
        // public void initMap() {
        // Map<String, Object> map = new HashMap<String, Object>();
        // map.put("providerClass", "IGoodsProvider");
        // map.put("providerClassPackage", "com.btg.provider.IGoodsProvider");
        // this.setMap(map);
        // }
        // };
        // mpg.setCfg(cfg);
        // 自定义模板配置，可以 copy 源码 mybatis-plus/src/main/resources/template 下面内容修改，
        // 放置自己项目的 src/main/resources/template 目录下, 默认名称一下可以不配置，也可以自定义模板名称
        // TemplateConfig tc = new TemplateConfig();
        // tc.setEntity("/tpl/entity.java.vm");
        // tc.setMapper("/tpl/mapper.java.vm");
        // tc.setXml("/tpl/mapper.xml.vm");
        // tc.setService("/tpl/service.java.vm");
        // tc.setController("/tpl/controller.java.vm");
        // mpg.setTemplate(tc);
        // 执行生成
        mpg.execute();

    }

    public static String[] getAllTableName() {

        List<String> tableNamesList = new ArrayList<String>();
        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://127.0.0.1:3306/zhzz?characterEncoding=utf8";
        String user = "root";
        String password = "root123456";

        try {
            Class.forName("com.mysql.jdbc.Driver");

            Connection conn = DriverManager.getConnection(
                    "jdbc:mysql://127.0.0.1:3306/zhzz?characterEncoding=utf8&user=htcuser&password=htcuserP@ssw0rd");
            ResultSet re = conn.createStatement().executeQuery("show tables;");
            while (re.next()) {
                System.out.println(re.getString("Tables_in_htcfund"));
                tableNamesList.add(re.getString("Tables_in_htcfund"));

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        int size = tableNamesList.size();
        String[] tableNames = new String[size];
        tableNamesList.toArray(tableNames);
        System.out.println(tableNames.toString());
        return tableNames;
    }
}
