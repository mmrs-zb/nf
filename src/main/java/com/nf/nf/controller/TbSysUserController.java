package com.nf.nf.controller;

import com.nf.nf.common.base.BaseController;
import com.nf.nf.common.base.BaseSearch;
import com.nf.nf.common.page.PageHelper;
import com.nf.nf.common.support.ApiData;
import com.nf.nf.model.TbSysUser;
import com.nf.nf.service.TbSysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 系统用户表  前端控制器
 * </p>
 *
 * @author cris
 * @since 2018-09-27
 */
@RestController
@RequestMapping("/tbSysUser")
@Api(value = "系统用户表接口", description = "系统用户表接口")
public class TbSysUserController extends BaseController {
	@Resource
    private TbSysUserService tbSysUserService;

	@ApiOperation(value = "分页查询系统用户表",response=TbSysUser.class)
	@PostMapping(value = "/read/list")
	public ApiData query(@RequestBody PageHelper param) {
		return getSuccessApiData(tbSysUserService.selectPageVo(param));
	}

	@ApiOperation(value = "系统用户表详情",response=TbSysUser.class)
	@PostMapping(value = "/read/detail/{id}")
	public ApiData get(@PathVariable String id) {
		return commonDetail(tbSysUserService,id);
	}

	@PostMapping(value = "update")
	@ApiOperation(value = "修改系统用户,id必传",response=TbSysUser.class)
	public ApiData update(@RequestBody TbSysUser param) {
		param.setPassword(null);
		param.setSalt(null);
		return commonUpdate(tbSysUserService,param);
	}

    @PostMapping(value = "insert")
    @ApiOperation(value = "新增系统用户",response=TbSysUser.class)
    public ApiData insert(@RequestBody TbSysUser param) {
		ApiData data = getSuccessApiData();
		int result = tbSysUserService.addUser(param);
		if (result == 0) {
			data.setResult(0);
			data.setMsg("新增失败，请联系管理员！");
		}else if (result == 1) {
			data.setData(result);
			data.setResult(1);
		} else if (result == 2) {
			data.setResult(0);
			data.setMsg("当前用户名在系统中已经存在！");
		}
		return data;
    }
}