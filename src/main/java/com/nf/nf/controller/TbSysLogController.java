package com.nf.nf.controller;

import com.nf.nf.common.base.BaseController;
import com.nf.nf.common.page.PageHelper;
import com.nf.nf.common.support.ApiData;
import com.nf.nf.model.TbSysLog;
import com.nf.nf.service.TbSysLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 日志表  前端控制器
 * </p>
 *
 * @author cris
 * @since 2018-10-29
 */
@RestController
@RequestMapping("/tbSysLog")
@Api(value = "日志表接口", description = "日志表接口")
public class TbSysLogController extends BaseController {
	@Resource
    private TbSysLogService tbSysLogService;

	@ApiOperation(value = "分页查询日志表",response=TbSysLog.class)
	@PostMapping(value = "/read/list")
	public ApiData query(@RequestBody PageHelper param) {
		return getSuccessApiData(tbSysLogService.selectPageVo(param));
	}

	@ApiOperation(value = "日志表详情",response=TbSysLog.class)
	@PostMapping(value = "/read/detail/{id}")
	public ApiData get(@PathVariable String id) {
		return commonDetail(tbSysLogService,id);
	}

	@PostMapping(value = "update")
	@ApiOperation(value = "修改日志表,id必传",response=TbSysLog.class)
	public ApiData update(@RequestBody TbSysLog param) {
		return commonUpdate(tbSysLogService,param);
	}

    @PostMapping(value = "insert")
    @ApiOperation(value = "新增日志表",response=TbSysLog.class)
    public ApiData insert(@RequestBody TbSysLog param) {
        param.setId(null);
        return commonInsert(tbSysLogService,param);
    }
}