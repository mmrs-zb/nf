package com.nf.nf.service;

import com.nf.nf.common.base.BaseService;
import com.nf.nf.mapper.TbSysLogMapper;
import com.nf.nf.model.TbSysLog;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 日志表  服务实现类
 * </p>
 *
 * @author cris
 * @since 2018-10-29
 */
@Service
public interface  TbSysLogService extends BaseService<TbSysLogMapper, TbSysLog>{

}