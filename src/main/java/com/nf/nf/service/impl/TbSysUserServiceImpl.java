package com.nf.nf.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nf.nf.common.base.BaseServiceImpl;
import com.nf.nf.common.core.Constants;
import com.nf.nf.mapper.TbSysUserMapper;
import com.nf.nf.model.TbSysLog;
import com.nf.nf.model.TbSysUser;
import com.nf.nf.service.TbSysUserService;
import com.nf.nf.util.IdGen;
import com.nf.nf.util.MD5Tools;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 系统用户表  服务实现类
 * </p>
 *
 * @author cris
 * @since 2018-09-27
 */
@Service
public class TbSysUserServiceImpl extends BaseServiceImpl<TbSysUserMapper, TbSysUser> implements TbSysUserService {
    @Resource
    private TbSysUserMapper tbSysUserMapper;

    @Override
    public IPage<TbSysUser> selectPageVo(Page page) {
        return tbSysUserMapper.selectPageVo(page);
    }

    @Override
    public Integer addUser(TbSysUser param) {
        TbSysUser tsu = new TbSysUser();
        tsu.setLoginName(param.getLoginName());
        QueryWrapper<TbSysUser> tsuQW = new QueryWrapper<TbSysUser>(tsu);
        TbSysUser tbSysUser = tbSysUserMapper.selectOne(tsuQW);
        if (null != tbSysUser) {
            return 2;
        }else{
            String id = IdGen.uuid();
            param.setId(id);
            param.setSalt(id);
            String pwdMd5 = MD5Tools.encodePassword(Constants.PASSWORD, id);
            param.setPassword(pwdMd5);
            Integer re = tbSysUserMapper.insert(param);
            if (1 == re ){
                return 1;
            }else{
                return 0;
            }
        }
    }
}