package com.nf.nf.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nf.nf.common.base.BaseServiceImpl;
import com.nf.nf.mapper.TbSysLogMapper;
import com.nf.nf.model.TbSysLog;
import com.nf.nf.service.TbSysLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * <p>
 * 日志表  服务实现类
 * </p>
 *
 * @author cris
 * @since 2018-10-29
 */
@Service
public class TbSysLogServiceImpl extends BaseServiceImpl<TbSysLogMapper, TbSysLog> implements TbSysLogService {
    @Resource
    private TbSysLogMapper TbSysLogMapper;

    @Override
    public IPage<TbSysLog> selectPageVo(Page page) {
        return TbSysLogMapper.selectPageVo(page);
    }
}