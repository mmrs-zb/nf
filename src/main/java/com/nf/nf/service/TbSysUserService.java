package com.nf.nf.service;

import com.nf.nf.model.TbSysUser;
import com.nf.nf.mapper.TbSysUserMapper;
import org.springframework.stereotype.Service;
import com.nf.nf.common.base.BaseService;

/**
 * <p>
 * 系统用户表  服务实现类
 * </p>
 *
 * @author cris
 * @since 2018-09-27
 */
public interface  TbSysUserService extends BaseService<TbSysUserMapper, TbSysUser>{
	/**
	 * @methodName: addUser
	 * @description: 新增用户
	 * <br/>
	  * @param param
	 * @return java.lang.Integer
	 * @author cris
	 * @date 2018/11/1 14:29
	 */
	public Integer addUser(TbSysUser param);
}