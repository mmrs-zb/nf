package com.nf.nf.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nf.nf.common.base.BaseMapper;
import com.nf.nf.common.page.PageHelper;
import com.nf.nf.model.TbSysLog;
import com.nf.nf.model.TbSysUser;

/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author cris
 * @since 2018-09-27
 */
public interface TbSysUserMapper extends BaseMapper<TbSysUser> {
    IPage<TbSysUser> selectPageVo(Page page);
}