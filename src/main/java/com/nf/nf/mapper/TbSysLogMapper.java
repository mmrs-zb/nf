package com.nf.nf.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nf.nf.common.base.BaseMapper;
import com.nf.nf.common.page.PageHelper;
import com.nf.nf.model.TbSysLog;

/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author cris
 * @since 2018-10-29
 */
public interface TbSysLogMapper extends BaseMapper<TbSysLog> {
    IPage<TbSysLog> selectPageVo(Page page);
}